<?php
require_once 'conexion.php';
require_once 'estudianteDao.php';
class estudiante{
    private $codigo;
    private $nombre;
    private $apellido;
    private $fecha_nacimiento;
    private $conexion;   
    private $estudianteDao;
    
    /**
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * @return string
     */
    public function getFecha_nacimiento()
    {
        return $this->fecha_nacimiento;
    }

    /**
     * @return Conexion
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    
    public function getEstudianteDao()
    {
        return $this->estudianteDao;
    }

    public function estudiante($codigo="", $nombre="", $apellido="", $fecha_nacimiento=""){
        $this -> codigo = $codigo;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> fecha_nacimiento = $fecha_nacimiento;
        $this -> conexion = new Conexion();
        $this -> estudianteDao = new estudianteDao($this -> codigo, $this -> nombre, $this -> apellido, $this -> fecha_nacimiento);
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDao -> consultar());
        $registro = $this -> conexion -> extraer();
        $aeropuertoOrigen = new Aeropuerto($registro[0]);
        $aeropuertoOrigen -> consultar();
        $this -> aeropuerto_origen = $aeropuertoOrigen;
        $aeropuertoDestino = new Aeropuerto($registro[1]);
        $aeropuertoDestino  -> consultar();
        $this -> aeropuerto_destino = $aeropuertoDestino;
        $administrador = new Administrador($registro[2]);
        $administrador -> consultar();
        $this -> administrador = $administrador;        
        $this -> conexion -> cerrar();
    }    

    public function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> estudianteDao -> crear());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> rutaDAO -> consultarTodos());
        $rutas = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $aeropuertoOrigen = new Aeropuerto($registro[1]);
            $aeropuertoOrigen -> consultar();
            $aeropuertoDestino = new Aeropuerto($registro[2]);
            $aeropuertoDestino  -> consultar();
            $administrador = new Administrador($registro[3]);
            $administrador -> consultar();
            $ruta = new Ruta($registro[0], $aeropuertoOrigen, $aeropuertoDestino, $administrador);
            array_push($rutas, $ruta);
        }
        $this -> conexion -> cerrar();
        return  $rutas;
    }
    
    
}
?>