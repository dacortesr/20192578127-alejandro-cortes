<?php
require_once 'estudiante.php';
if(isset($_POST["crear"])){
    $estu = new estudiante($_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["fecha_nacimento"]);
    $estu -> crear();
}
?>

<div class="container">
	<div class="row mt-3">
		<div class="col-4"></div>
		<div class="col-4">
			<div class="card">
				<h5 class="card-header">Añadir estudiante </h5>
				<div class="card-body">
					<?php if(isset($_POST["crear"])) { ?>
					<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							Datos registrados correctamente
							<button type="button" class="btn-close" data-bs-dismiss="alert"
								aria-label="Close"></button>
					</div>
					<?php } ?>				
					<form method="post" action="index.php?pid=<?php echo base64_encode("crear_estudainte.php")?>" >
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">Codigo</label>
							<input type="text" class="form-control" name="codigo" required="required">							
						</div>
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">Nombre</label>
							<input type="text" class="form-control" name="nombre" required="required">
						</div>
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">Apellido</label>
							<input type="text" class="form-control" name="apellido" required="required">
						</div>
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">Fecha de nacimiento</label>
							<input type="date" class="form-control" name="fecha_nacimiento" required="required">
						</div>
						<button type="submit" class="btn btn-primary" name="crear">Añadir estudiante</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>